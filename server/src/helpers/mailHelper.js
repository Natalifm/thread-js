/* eslint-disable */
import { transporter } from '../config/mailConfig';

export const sendSharedLinkEmail = ({ email, link }) => {
  const mailOptions = {
    from: 'miniTwitB@gmail.com',
    to: `${email}`,
    subject: 'Shared post from Thread',
    text: `Hello user with email ${email} shared a post with You.\n`
      + `\n Have a look: ${link}`
  };

  return transporter.sendMail(mailOptions);
};

export const sendLikeNotificationEmail = email => {
  const mailOptions = {
    from: 'miniTwitB@gmail.com',
    to: `${email}`,
    subject: 'Thread - Your post was liked!',
    text: `Hello, Your post was liked by user with email: ${email}!`
  };

  transporter.sendMail(mailOptions, (err, response) => (err ? ({
    error: true,
    message: `there was an error: ${err}`
  }) : ({
    error: false,
    message: `here is the response: ${response}`
  })));
};
