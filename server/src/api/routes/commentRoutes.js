/* eslint-disable */
import { Router } from 'express';
import * as commentService from '../services/commentService';

const router = Router();

router
  .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  // .put('/', (req, res, next) => commentService.update(req.body)
  //   .then(comment => res.send(comment))
  //   .catch(next))

  .delete('/', (req, res, next) => commentService.deleteCommentById(req.body.id)
    .then(result => {
      if (result) {
        res.send(req.body);
      }
    })
    .catch(next))
  .put('/', (req, res, next) => commentService.update(req.body)
    .then(comment => res.send(comment))
    .catch(next))

  //for like/dis


export default router;
