/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import {
  likePost,
  dislikePost,
  addComment,
  getCommentLikes,
  likeComment,
  dislikeComment,
  toggleExpandedPost,
  toggleExpandedEditComment,
  deleteComment
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

import ExpandedEditComment from "src/containers/ExpandedEditComment";

const ExpandedPost = ({
  post,
  sharePost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  addComment: add,
  expandedEditComment,
  toggleExpandedEditComment,
  deleteComment,
  user,
  likeComment,
  dislikeComment,
  getCommentLikes
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {post
      ? (
        <Modal.Content>
          <Post
            post={post}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <Header as="h3" dividing>
              Comments
            </Header>
            {post.comments && post.comments
              .sort((c1, c2) => moment(c1.createdAt)
                .diff(c2.createdAt))
              .map(comment => (
                <Comment
                  key={comment.id}
                  comment={comment}
                  likeComment={likeComment}
                  dislikeComment={dislikeComment}
                  getLikes={getCommentLikes}
                  deleteComment={
                    user.id === comment.userId ? deleteComment : undefined
                  }
                  toggleExpandedEditComment={
                    user.id === comment.userId ? toggleExpandedEditComment : undefined
                  }
                />
              ))}
            <AddComment postId={post.id} addComment={add} />
          </CommentUI.Group>
        </Modal.Content>
      )
      : <Spinner />}

    {expandedEditComment && <ExpandedEditComment />}
  </Modal>
);

ExpandedPost.defaultProps = {
  expandedEditComment: undefined,
};

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedEditComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,

  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  getCommentLikes: PropTypes.func.isRequired,
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost,
  user: rootState.profile.user,
  expandedEditComment: rootState.posts.expandedEditComment
});

const actions = {
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment,
  toggleExpandedEditComment,
  deleteComment,

  likeComment,
  dislikeComment,
  getCommentLikes
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
