/* eslint-disable */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import {
  Grid,
  Image,
  Input,
  Button,
  Dimmer,
  Header
} from 'semantic-ui-react';

import ExpandedEditUsername from '../ExpandedEditUsername';
import ExpandedEditStatus from '../ExpandedEditStatus';
import ExpandedEditImage from '../ExpandedEditImage';
import {
  toggleExpandedEditUsernameProfile,
  toggleExpandedEditStatusProfile,
  toggleExpandedEditImageProfile
} from './actions';
import * as imageService from '../../services/imageService';

const Profile = ({
  user,
  expandedEditUsernameProfile,
  expandedEditStatusProfile,
  expandedEditImageProfile,
  toggleExpandedEditUsernameProfile: toggleUsername,
  toggleExpandedEditStatusProfile: toggleStatus,
  toggleExpandedEditImageProfile: toggleImage
}) => {
  const [active, setActive] = useState(false);

  const handleShow = () => setActive(true);
  const handleHide = () => setActive(false);

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <Dimmer.Dimmable
          as={Image}
          size="medium"
          centered
          circular
          blurring
          dimmed={active}
          onMouseEnter={handleShow}
          onMouseLeave={handleHide}
        >
          <Image centered src={getUserImgLink(user.image)} size="medium" circular/>
          <Dimmer active={active}>
            <Header as='h3' inverted>
              Update Image
            </Header>
            <Button inverted onClick={toggleImage}>Update</Button>
          </Dimmer>
        </Dimmer.Dimmable>
        <br/>
        <br/>
        <Input
          icon="user"
          iconPosition="left"
          placeholder="Username"
          type="text"
          disabled
          value={user.username}
          label={<Button icon="edit" onClick={toggleUsername}></Button>}
          labelPosition="right"
        />
        <br/>
        <br/>
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={user.email}
        />
        <br/>
        <br/>
        <Input
          icon="compose"
          iconPosition="left"
          placeholder="Status"
          type="text"
          disabled
          value={user.status}
          label={<Button icon="edit" onClick={toggleStatus}></Button>}
          labelPosition="right"
        />
      </Grid.Column>
      {expandedEditUsernameProfile && <ExpandedEditUsername/>}
      {expandedEditStatusProfile && <ExpandedEditStatus/>}
      {expandedEditImageProfile && <ExpandedEditImage uploadImage={uploadImage} />}
    </Grid>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  expandedEditUsernameProfile: PropTypes.objectOf(PropTypes.any),
  toggleExpandedEditUsernameProfile: PropTypes.func.isRequired,
  expandedEditStatusProfile: PropTypes.objectOf(PropTypes.any),
  toggleExpandedEditStatusProfile: PropTypes.func.isRequired,
  toggleExpandedEditImageProfile: PropTypes.func.isRequired,
  expandedEditImageProfile: PropTypes.objectOf(PropTypes.any)
};

Profile.defaultProps = {
  user: {},
  expandedEditUsernameProfile: undefined,
  expandedEditStatusProfile: undefined,
  expandedEditImageProfile: undefined,
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user,
  expandedEditUsernameProfile: rootState.profile.expandedEditUsernameProfile,
  expandedEditStatusProfile: rootState.profile.expandedEditStatusProfile,
  expandedEditImageProfile: rootState.profile.expandedEditImageProfile,
});

const actions = {
  toggleExpandedEditUsernameProfile,
  toggleExpandedEditStatusProfile,
  toggleExpandedEditImageProfile
};

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
