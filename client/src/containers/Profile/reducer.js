/* eslint-disable */
import {
  SET_USER,
  SET_ERROR,
  SET_USER_BY_TOKEN,
  SET_EXPANDED_EDIT_USERNAME_PROFILE,
  EDIT_PROFILE,
  GET_RESOLVE, SET_EXPANDED_EDIT_STATUS_PROFILE, SET_EXPANDED_EDIT_IMAGE_PROFILE
} from './actionTypes';

export default (state = {resolve: undefined}, action) => {
  switch (action.type) {
    case GET_RESOLVE:
      return {
        ...state,
        resolve: action.resolve
      }
    case SET_USER:
      return {
        ...state,
        user: action.user,
        isAuthorized: Boolean(action.user?.id),
        isLoading: false
      }
    case SET_USER_BY_TOKEN:
      return {
        ...state,
        userByToken: action.user,
        isLoading: false
      }
    case SET_ERROR:
      return {
        ...state,
        error: action.error
      }
    case SET_EXPANDED_EDIT_USERNAME_PROFILE:
      return {
        ...state,
        expandedEditUsernameProfile: action.user
      }
    case EDIT_PROFILE:
      return {
        ...state,
        user: action.user
      }
    case SET_EXPANDED_EDIT_IMAGE_PROFILE:
      return {
        ...state,
        expandedEditImageProfile: action.user
      };
    case SET_EXPANDED_EDIT_STATUS_PROFILE:
      return {
        ...state,
        expandedEditStatusProfile: action.user
      }
    default:
      return state;
  }
};
