/* eslint-disable */
import * as authService from 'src/services/authService';
import {
  SET_USER,
  SET_ERROR,
  SET_USER_BY_TOKEN,
  SET_EXPANDED_EDIT_USERNAME_PROFILE,
  EDIT_PROFILE,
  SET_EXPANDED_EDIT_STATUS_PROFILE,
  SET_EXPANDED_EDIT_IMAGE_PROFILE
} from './actionTypes';

const setToken = token => localStorage.setItem('token', token);

const setUser = user => async dispatch => dispatch({
  type: SET_USER,
  user
});

const setError = error => ({
  type: SET_ERROR,
  error
});

const setUserByToken = user => ({
  type: SET_USER_BY_TOKEN,
  user
});

const setAuthData = (user = null, token = '') => (dispatch, getRootState) => {
  setToken(token); // token should be set first before user
  setUser(user)(dispatch, getRootState);
};

const handleAuthResponse = authResponsePromise => async (dispatch, getRootState) => {
  const { user, token } = await authResponsePromise;
  setAuthData(user, token)(dispatch, getRootState);
};

export const login = request => handleAuthResponse(authService.login(request));

export const register = request => handleAuthResponse(authService.registration(request));

export const logout = () => setAuthData();

export const loadCurrentUser = () => async (dispatch, getRootState) => {
  const user = await authService.getCurrentUser();
  setUser(user)(dispatch, getRootState);
};

const setExpandedEditUsernameProfileAction = user => ({
  type: SET_EXPANDED_EDIT_USERNAME_PROFILE,
  user
});

export const toggleExpandedEditUsernameProfile = user => async dispatch => {
  const current = user ? await authService.getCurrentUser() : undefined;
  dispatch(setExpandedEditUsernameProfileAction(current));
};

export const setExpandedEditStatusProfileAction = user => ({
  type: SET_EXPANDED_EDIT_STATUS_PROFILE,
  user
});


export const toggleExpandedEditStatusProfile = user => async dispatch => {
  const current = user ? await authService.getCurrentUser() : undefined;
  dispatch(setExpandedEditStatusProfileAction(current));
};

const setExpandedEditImageProfileAction = user => ({
  type: SET_EXPANDED_EDIT_IMAGE_PROFILE,
  user
});

export const toggleExpandedEditImageProfile = user => async dispatch => {
  const current = user ? await authService.getCurrentUser() : undefined;
  dispatch(setExpandedEditImageProfileAction(current));
};


const editProfileAction = user => ({
  type: EDIT_PROFILE,
  user
});

export const editProfile = user => async dispatch => {
  try {
    const { id } = await authService.updateUser(user);
    const updatedProfile = await authService.getCurrentUser();
    dispatch(editProfileAction(updatedProfile));
    dispatch(setError(false));
  } catch(err) {
    dispatch(toggleExpandedEditUsernameProfile(user));
    dispatch(setError(true));
  }

}
