/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';
import { Card, Comment as CommentUI, Icon, Label } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';
import { connect } from 'react-redux';

const Comment = ({
  comment,
  deleteComment,
  toggleExpandedEditComment,
  likeComment,
  dislikeComment
}) => {
  const {
    id,
    body,
    createdAt,
    user,
    likeCount,
    dislikeCount
  } = comment;
  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)}/>
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {user.status}
        </CommentUI.Metadata>
        <CommentUI.Metadata>
          {moment(createdAt)
            .fromNow()}
        </CommentUI.Metadata>
        <CommentUI.Text>
          {body}
        </CommentUI.Text>
        <CommentUI.Content>
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likeComment(id)}>
            <Icon name="thumbs up"/>
            {likeCount}
          </Label>
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikeComment(id)}>
            <Icon name="thumbs down"/>
            {dislikeCount}
          </Label>
          {
            toggleExpandedEditComment || deleteComment ? (
              <span>
                  {toggleExpandedEditComment &&
                  <Label basic as="a" size="small" className={styles.toolbarBtn}
                         onClick={() => toggleExpandedEditComment(id)}>
                    <Icon name="edit"/>
                  </Label>
                  }
                {deleteComment &&
                <Label basic as="a" size="small" className={styles.toolbarBtn} onClick={() => deleteComment(comment)}>
                  <Icon name="remove"/>
                </Label>
                }
                </span>
            ) : null
          }
        </CommentUI.Content>
      </CommentUI.Content>
    </CommentUI>
  );
};

const mapStateToProps = rootState => ({
  commentLikes: rootState.posts.commentLikes
});

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired
};

export default connect(mapStateToProps)(Comment);
